import { ChapterKey } from './classes/ChapterKey'

export type HtmlString = string
export type DocVersionKey = string

export type AmendingInstruction =
  | DeleteInstruction
  | InsertInstruction
  | RenumberInstruction
  | RenumberMultipleInstruction
  | ReplaceInstruction
  | ReplacePartlyInstruction
  | ReplaceKeywordInstruction

export type DeleteInstruction = {
  type: 'DELETE'
  targetChapterKey: ChapterKey
}

export type InsertInstruction = {
  type: 'INSERT'
  targetChapterKey: ChapterKey
  newContent: HtmlString
}

export type RenumberInstruction = {
  type: 'RENUMBER'
  targetChapterKey: ChapterKey
  newChapterKey: ChapterKey
}

export type RenumberMultipleInstruction = {
  type: 'RENUMBER_MULTIPLE'
  formerKeys: { from: ChapterKey; to: ChapterKey }
  newKeys: { from: ChapterKey; to: ChapterKey }
}

export type ReplaceInstruction = {
  type: 'REPLACE'
  targetChapterKey: ChapterKey
  newContent: HtmlString
}

export type ReplacePartlyInstruction = {
  type: 'REPLACE_PARTLY'
  targetChapterKey: ChapterKey
  newContent: (Ellipsis | HtmlString)[]
}

export type ReplaceKeywordInstruction = {
  type: 'REPLACE_KEYWORD'
  targetChapterKey: ChapterKey
  keyword: string
  replacement: string
  recursive: boolean
}

export type Ellipsis = '...'
