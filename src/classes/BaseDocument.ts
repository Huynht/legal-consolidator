import {
  AmendingInstruction,
  DeleteInstruction,
  DocVersionKey,
  InsertInstruction,
  RenumberInstruction,
  ReplaceInstruction,
  ReplaceKeywordInstruction,
  ReplacePartlyInstruction,
} from '../types'
import { DomUtil } from '../utils/domUtil'
import { matchEllipsis } from '../utils/ellipsisMatcher'
import { ChapterKey } from './ChapterKey'

export class BaseDocument {
  // ATTRIBUTES
  html: Document
  docVersionKey: DocVersionKey

  // CONSTRUCTOR
  constructor(input: { html: Document; docVersionKey: DocVersionKey }) {
    this.html = input.html
    this.docVersionKey = input.docVersionKey
  }

  // METHODS
  amend(instruction: AmendingInstruction, originalKeys?: Map<string, Element>) {
    if (
      (instruction.type === 'RENUMBER' ||
        instruction.type === 'RENUMBER_MULTIPLE') &&
      !originalKeys
    ) {
      console.error('no original keys map provided')
      return
    }

    switch (instruction.type) {
      case 'DELETE':
        this.deleteChapterByKey(instruction)
        break
      case 'INSERT':
        this.insertNewChapterString(instruction)
        break
      case 'RENUMBER':
        this.renumberChapter(instruction, originalKeys!)
        break
      case 'REPLACE':
        this.replaceChapter(instruction)
        break
      case 'REPLACE_PARTLY':
        this.replaceChapterPartly(instruction)
        break
      case 'REPLACE_KEYWORD':
        this.replaceKeyword(instruction)
        break
      default:
        throw 'Unhandled instruction type!'
    }
  }

  findByChapterKey(chapterKey: ChapterKey) {
    const queriedElements = this.html.querySelectorAll(
      `[chapter-key='${chapterKey.value}']`
    )

    if (queriedElements.length > 1) {
      console.error(
        `There are multiple chapters with the same identifier (${chapterKey.value}). This is an error and needs to be fixed first.`
      )
      return undefined
    }
    if (queriedElements.length === 0) {
      console.error(
        `[FindByChapterKey] No chapter with the identifier ${chapterKey.value} was found. Please make sure that the base document and the amending instruction is correct!`
      )
      return undefined
    }

    return queriedElements[0] as HTMLElement
  }

  findRootChapterFor(chapterKey: ChapterKey) {
    const queriedElements = this.html.querySelectorAll(
      `[chapter-key='${chapterKey.value}']`
    )

    if (queriedElements.length > 1) {
      throw `There are multiple chapters with the same identifier (${chapterKey.value}). This is an error and needs to be fixed first.`
    }
    if (queriedElements.length === 0) {
      throw `[FindRootChapterFor] No chapter with the identifier ${chapterKey.value} was found. Please make sure that the base document and the amending instruction is correct!`
    }

    return queriedElements[0]
  }

  deleteChapterByKey(instruction: DeleteInstruction) {
    const chapterToBeRemoved = this.findByChapterKey(
      instruction.targetChapterKey
    )
    if (!chapterToBeRemoved) {
      console.error(
        `[DeleteChapterByKey] Chapter ${instruction.targetChapterKey.value} not found`
      )
      return
    }

    console.info(
      `removing chapter with key: ${instruction.targetChapterKey.value}`
    )
    chapterToBeRemoved.remove()
  }

  // TODO: prevent INSERT of duplicate chapterKey
  insertNewChapterString(instruction: InsertInstruction) {
    const newChapterElement = DomUtil.parseFromHtmlString(
      `<div>${instruction.newContent}</div>`
    )

    this.insertNewChapterElement(
      instruction.targetChapterKey,
      newChapterElement
    )
  }

  insertNewChapterElement(
    newChapterKey: ChapterKey,
    newChapterElement: Element
  ) {
    newChapterElement.setAttribute('chapter-key', newChapterKey.value)

    let youngerSiblingKey = ChapterKey.generateYoungerSiblingKey(newChapterKey)

    if (youngerSiblingKey) {
      let youngerSibling = this.findByChapterKey(youngerSiblingKey)

      while (!youngerSibling) {
        youngerSiblingKey = ChapterKey.generateYoungerSiblingKey(
          youngerSiblingKey!
        )

        if (!youngerSiblingKey) {
          const parentKey = ChapterKey.generateParentKey(newChapterKey)
          if (parentKey) {
            console.info(
              `inserting new chapter ${newChapterKey.value} UNDER node with key: ${parentKey.value}`
            )
            this.insertUnderParent(parentKey, newChapterElement, newChapterKey)
          }
          return
        }

        youngerSibling = this.findByChapterKey(youngerSiblingKey)
      }

      youngerSibling.after(newChapterElement)
      return
    }

    const parentKey = ChapterKey.generateParentKey(newChapterKey)
    if (parentKey) {
      this.insertUnderParent(parentKey, newChapterElement, newChapterKey)
    }
  }

  insertUnderParent(
    parentKey: ChapterKey,
    newChapterElement: Element,
    newChapterKey: ChapterKey
  ) {
    const parent = this.findByChapterKey(parentKey)
    if (!parent) {
      console.error(
        `[InsertNewChapter] Parent ${parentKey.value} not found for chapter to be inserted ${newChapterKey.value}`
      )
      return
    }

    const firstChildChapter = parent.querySelector('[chapter-key]')
    if (firstChildChapter) {
      console.info(
        `inserting new chapter under node with key: ${parentKey.value}`
      )
      firstChildChapter.before(newChapterElement)
      return
    } else {
      console.info(
        `inserting new chapter as first child under node with key ${parentKey.value}`
      )

      parent.appendChild(newChapterElement)
      return
    }
  }

  // TODO: prevent renumbering to an existing chapterKey
  renumberChapter(
    instruction: RenumberInstruction,
    originalKeys: Map<string, Element>
  ) {
    const targetChapterKey = instruction.targetChapterKey
    const newChapterKey = instruction.newChapterKey

    const targetChapter = originalKeys.get(targetChapterKey.value)
    if (!targetChapter) {
      console.error(
        `Failed to renumber chapter ${instruction.targetChapterKey.value} to ${newChapterKey.value}: Could not find chapter in DOM.`
      )
      return
    }

    targetChapter.innerHTML = targetChapter.innerHTML.replace(
      ChapterKey.getNumbering(targetChapterKey),
      ChapterKey.getNumbering(newChapterKey)
    )

    this.insertNewChapterElement(newChapterKey, targetChapter)
  }

  replaceChapter(instruction: ReplaceInstruction) {
    const targetChapter = this.findByChapterKey(instruction.targetChapterKey)
    if (!targetChapter) {
      console.error(
        `Failed to replace content of chapter ${instruction.targetChapterKey.value}: Could not find chapter in DOM.`
      )
      return
    }

    const children: Element[] = []
    for (const child of targetChapter.children) {
      if (child.getAttribute('chapter-key')) {
        children.push(child)
      }
    }

    targetChapter.innerHTML = `<div>${instruction.newContent}</div>`

    children.forEach((child) => {
      targetChapter.appendChild(child)
    })
  }

  replaceChapterPartly(instruction: ReplacePartlyInstruction) {
    const targetChapterKey = instruction.targetChapterKey

    const targetChapter = this.findByChapterKey(targetChapterKey)
    if (!targetChapter) {
      console.error(
        `Failed to partly replace chapter content of ${instruction.targetChapterKey.value}: Could not find chapter in DOM.`
      )
      return
    }

    const oldHtml = targetChapter.innerHTML

    const children: Element[] = []
    for (const child of targetChapter.children) {
      if (child.getAttribute('chapter-key')) {
        children.push(child)
      }
    }

    let finalChapterContent = ''
    instruction.newContent.forEach((newContentPart, i) => {
      if (newContentPart === '...') {
        const matchingText = matchEllipsis(oldHtml, {
          before: instruction.newContent[i - 1],
          after: instruction.newContent[i + 1],
        })

        finalChapterContent += matchingText
      } else {
        if (i === 0 && !newContentPart.startsWith('<')) {
          finalChapterContent += '<p>' + newContentPart
          return
        }

        if (
          i === instruction.newContent.length - 1 &&
          !newContentPart.endsWith('>')
        ) {
          finalChapterContent += newContentPart + '</p>'
          return
        }

        finalChapterContent += newContentPart
        return
      }
    })

    targetChapter.innerHTML = finalChapterContent

    children.forEach((child) => {
      targetChapter.appendChild(child)
    })
  }

  replaceKeyword(instruction: ReplaceKeywordInstruction) {
    let targetChapter
    if (instruction.targetChapterKey.value === 'ROOT') {
      targetChapter = this.html.getElementsByTagName('BODY')[0]
    } else {
      targetChapter = this.findByChapterKey(instruction.targetChapterKey)
    }

    if (!targetChapter) {
      console.error(
        `Failed to replace keyword "${instruction.keyword}" with "${instruction.replacement}" in chapter ${instruction.targetChapterKey.value}: Could not find chapter in DOM.`
      )
      return
    }

    if (instruction.recursive) {
      targetChapter.innerHTML = targetChapter.innerHTML.replaceAll(
        instruction.keyword,
        instruction.replacement
      )
    } else {
      // iterate text nodes

      const targetRegex = new RegExp(`\\b${instruction.keyword}\\b`, 'g')

      targetChapter.childNodes.forEach((child) => {
        if (child.nodeType === 3) {
          if (child.nodeValue) {
            child.nodeValue = child.nodeValue.replace(
              targetRegex,
              instruction.replacement
            )
          }
        }
      })

      for (const child of targetChapter.children) {
        if (!child.getAttribute('chapter-key')) {
          child.innerHTML = child.innerHTML.replace(
            targetRegex,
            instruction.replacement
          )
        }
      }
    }
  }
}
