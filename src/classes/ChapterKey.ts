import { StrUtils } from '../utils/strUtils'

export class ChapterKey {
  // ATTRIBUTES
  value: string

  // ACCESSORS
  static getRootChapter(input: ChapterKey): string {
    const keyParts = ChapterKey.splitAtChapterSynonym(input)
    if (keyParts.length === 1) {
      // chapter key is either part of the original document or a root chapter
      const identifierRegex = /^(\d+.)+$/g
      if (identifierRegex.test(input.value)) {
        return input.value.split('.')[0] + '.'
      } else {
        return 'root' as const
      }
    }

    return keyParts[0]
  }

  static getNumbering(input: ChapterKey): string {
    const keyParts = ChapterKey.splitAtChapterSynonym(input)
    return keyParts[keyParts.length - 1]
  }

  static generateParentKey(input: ChapterKey): ChapterKey | undefined {
    if (ChapterKey.getRootChapter(input) === 'root') return undefined

    const numberingParts = ChapterKey.getNumbering(input).split('.')
    let parentNumbering: string
    // if numbering ends on .
    if (numberingParts[numberingParts.length - 1] === '') {
      parentNumbering = numberingParts
        .slice(0, numberingParts.length - 2)
        .concat([''])
        .join('.')
    } else {
      parentNumbering = numberingParts
        .slice(0, numberingParts.length - 1)
        .join('.')
    }

    if (ChapterKey.getChapterSynonym(input)) {
      return new ChapterKey(
        `${ChapterKey.getRootChapter(input)} ${ChapterKey.getChapterSynonym(
          input
        )} ${parentNumbering}`
      )
    } else {
      return new ChapterKey(parentNumbering)
    }
  }

  static generateYoungerSiblingKey(input: ChapterKey): ChapterKey | undefined {
    const numberingParts = ChapterKey.getNumbering(input).split('.')
    const lastIndex = (() => {
      if (numberingParts[numberingParts.length - 1] === '') {
        return numberingParts.length - 2
      }

      return numberingParts.length - 1
    })()

    const lastNumber = parseInt(numberingParts[lastIndex])
    if (lastNumber == 1) return undefined
    numberingParts[lastIndex] = `${lastNumber - 1}`

    if (ChapterKey.getChapterSynonym(input)) {
      return new ChapterKey(
        `${ChapterKey.getRootChapter(input)} ${ChapterKey.getChapterSynonym(
          input
        )} ${numberingParts.join('.')}`
      )
    } else {
      return new ChapterKey(numberingParts.join('.'))
    }
  }

  // CONSTRUCTOR
  constructor(value: string) {
    this.value = value
  }

  // METHODS
  static _chapterSynonyms = ['item', 'paragraph', 'chapter']
  static chapterSynonyms = ChapterKey._chapterSynonyms
    .map((synonym) => {
      return [synonym, StrUtils.capitalizeFirst(synonym)]
    })
    .flat()

  static splitAtChapterSynonym(input: ChapterKey) {
    for (let i = 0; i < ChapterKey.chapterSynonyms.length; i++) {
      const synonym = ChapterKey.chapterSynonyms[i]

      if (input.value.includes(synonym)) {
        return input.value.split(` ${synonym} `)
      }
    }

    return [input.value]
  }

  static getChapterSynonym(input: ChapterKey) {
    for (let i = 0; i < ChapterKey.chapterSynonyms.length; i++) {
      const synonym = ChapterKey.chapterSynonyms[i]

      if (input.value.includes(synonym)) {
        return synonym
      }
    }

    return undefined
  }
}
