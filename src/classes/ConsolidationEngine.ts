import { JSDOM } from 'jsdom'
import {
  AmendingInstruction,
  DeleteInstruction,
  DocVersionKey,
  HtmlString,
  InsertInstruction,
  RenumberInstruction,
  RenumberMultipleInstruction,
} from '../types'
import { BaseDocument } from './BaseDocument'
import { ChapterKey } from './ChapterKey'

export class ConsolidationEngine {
  // ATTRIBUTES
  baseDocuments: Map<DocVersionKey, BaseDocument> = new Map()

  // METHODS
  declareBaseDocument(docVersionKey: DocVersionKey, htmlString: HtmlString) {
    const html = new JSDOM(htmlString).window.document

    this.baseDocuments.set(
      docVersionKey,
      new BaseDocument({ html, docVersionKey })
    )
  }

  consolidate(
    docVersionKey: DocVersionKey,
    newVersionKey: DocVersionKey,
    amendingInstructions: AmendingInstruction[]
  ): BaseDocument {
    const currentDoc = this.baseDocuments.get(docVersionKey)

    if (!currentDoc)
      throw `Document with version key ${docVersionKey} could not be found.`

    const originalKeys = new Map<string, Element>()
    const chapters = currentDoc.html.querySelectorAll('[chapter-key]')

    chapters.forEach((chapter) => {
      const chapterKeyString = chapter.getAttribute('chapter-key')
      if (chapterKeyString) {
        originalKeys.set(chapterKeyString, chapter)
      }
    })

    let newDoc: BaseDocument = new BaseDocument({
      html: new JSDOM(currentDoc.html.documentElement.innerHTML).window
        .document,
      docVersionKey: newVersionKey,
    })

    const deleteOperations = amendingInstructions.filter(
      (instruction) => instruction.type === 'DELETE'
    )

    const renumberMultipleOperations = amendingInstructions.filter(
      (instruction) => instruction.type === 'RENUMBER_MULTIPLE'
    ) as RenumberMultipleInstruction[]
    const splitRenumberMultipleOperations = splitRenumberMultipleInstructions(
      renumberMultipleOperations
    )

    const renumberOperations = amendingInstructions
      .filter((instruction) => instruction.type === 'RENUMBER')
      .concat(splitRenumberMultipleOperations) as RenumberInstruction[]

    const rest = amendingInstructions.filter(
      (instruction) =>
        instruction.type !== 'DELETE' &&
        instruction.type !== 'RENUMBER' &&
        instruction.type !== 'RENUMBER_MULTIPLE'
    )

    let i = 0
    function applyToBaseDoc(instruction: AmendingInstruction) {
      console.info(
        'applying amendment #' +
          i +
          ', ' +
          instruction.type +
          ' ' +
          // @ts-ignore
          instruction.targetChapterKey.value
      )
      i++
      newDoc.amend(instruction, originalKeys)
    }
    deleteOperations.forEach(applyToBaseDoc)
    handleRenumberInstructions(newDoc, renumberOperations)
    rest.forEach(applyToBaseDoc)

    return newDoc
  }
}

function handleRenumberInstructions(
  baseDoc: BaseDocument,
  instructions: RenumberInstruction[]
) {
  const deleteInstructions: DeleteInstruction[] = []
  const insertInstructions: InsertInstruction[] = []

  instructions.forEach((renumberInstruction) => {
    const chapterElement = baseDoc.findByChapterKey(
      renumberInstruction.targetChapterKey
    )

    if (!chapterElement) {
      console.error('meeeeh')
      return
    }

    deleteInstructions.push({
      type: 'DELETE',
      targetChapterKey: renumberInstruction.targetChapterKey,
    })

    insertInstructions.push({
      type: 'INSERT',
      targetChapterKey: renumberInstruction.newChapterKey,
      newContent: chapterElement.innerHTML.replaceAll(
        ChapterKey.getNumbering(renumberInstruction.targetChapterKey),
        ChapterKey.getNumbering(renumberInstruction.newChapterKey)
      ),
    })
  })
  const dlt = deleteInstructions.map(
    (instruction) => instruction.targetChapterKey.value
  )
  const ins = insertInstructions.map(
    (instruction) => instruction.targetChapterKey.value
  )

  deleteInstructions.forEach((deleteInstruction, i) => {
    baseDoc.amend(deleteInstruction)
  })

  insertInstructions.forEach((insertInstruction) => {
    baseDoc.amend(insertInstruction)
  })
}

function splitRenumberMultipleInstructions(
  instructions: RenumberMultipleInstruction[]
): RenumberInstruction[] {
  const output: RenumberInstruction[] = []

  instructions.forEach((instruction) => {
    let currentFormerKey = instruction.formerKeys.to
    let formerKeys: ChapterKey[] = [currentFormerKey]
    while (true) {
      // we iterate front to back (to -> from) in order to guarantee that generateYoungerSiblingKey
      // will eventually return undefined and the while loop will break
      const nextKey = ChapterKey.generateYoungerSiblingKey(currentFormerKey)
      if (!nextKey) break

      formerKeys.push(nextKey)
      currentFormerKey = nextKey

      if (nextKey.value === instruction.formerKeys.from.value) break
    }
    formerKeys.reverse()

    let currentNewKey = instruction.newKeys.to
    let newKeys: ChapterKey[] = [currentNewKey]
    while (true) {
      const nextKey = ChapterKey.generateYoungerSiblingKey(currentNewKey)
      if (!nextKey) break

      newKeys.push(nextKey)
      currentNewKey = nextKey

      if (nextKey.value === instruction.newKeys.from.value) break
    }
    newKeys.reverse()

    if (formerKeys.length !== newKeys.length) {
      console.error(
        `Former key range and new key range seem to contain different amounts of keys. \nFormer: ${formerKeys.length} from: ${instruction.formerKeys.from.value} to: ${instruction.formerKeys.to.value} \nNew: ${newKeys.length} from: ${instruction.formerKeys.from.value} to: ${instruction.formerKeys.to.value}`
      )
      return
    }

    for (let i = 0; i < formerKeys.length; i++) {
      console.info(`renumbering ${formerKeys[i].value} to ${newKeys[i].value}`)
      output.push({
        type: 'RENUMBER',
        targetChapterKey: formerKeys[i],
        newChapterKey: newKeys[i],
      })
    }
  })

  return output
}
