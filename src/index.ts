export * from './classes/BaseDocument'
export * from './classes/ChapterKey'
export * from './classes/ConsolidationEngine'
export { ConsolidationEngine } from './classes/ConsolidationEngine'
export * from './types'
;(window as any).global = window
