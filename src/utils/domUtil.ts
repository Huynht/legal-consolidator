import { ObjectId } from 'bson'
import { JSDOM } from 'jsdom'
import { HtmlString } from '../types'

function parseFromHtmlString(input: HtmlString): Element {
  const window = new JSDOM().window
  const domParser = new window.DOMParser()
  const newElement = domParser.parseFromString(input, 'text/html').body
    .firstElementChild

  if (!newElement) throw 'Something went wrong when creating a new DOM Element'

  const objectId = new ObjectId()
  newElement.id = objectId.toString()

  return newElement
}

function getSiblingIndex(input: Element) {
  const parent = input.parentNode
  if (!parent) {
    return
  }
  return Array.prototype.indexOf.call(parent.children, input)
}

function getAllChildrenDF(input: Element) {
  let output: Element[] = []

  for (const child of input.children) {
    output.push(child)
    if (child.children && child.children.length > 0) {
      output = output.concat(getAllChildrenDF(child))
    }
  }

  return output
}

export const DomUtil = {
  parseFromHtmlString,
  getSiblingIndex,
  getAllChildrenDF,
}
