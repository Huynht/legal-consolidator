import _ from 'lodash'

export function matchEllipsis(
  originalText: string,
  beforeAfterEllipsisText: { before?: string; after?: string }
) {
  const beforeEllipsis = beforeAfterEllipsisText.before
  const afterEllipsis = beforeAfterEllipsisText.after

  if (!beforeEllipsis && !afterEllipsis) {
    console.error('Ellipsis need to be preceded by or followed by text.')
  }

  const removedWhitespace = originalText
    .replaceAll(/\s+/g, ' ')
    .replaceAll('\n', '')
  const stepSize = 4

  // if no
  let startAnchorIndex: number = 0
  let endAnchorIndex: number = removedWhitespace.length - 1

  // FIND START ANCHOR
  if (beforeEllipsis) {
    const steps = Math.ceil(beforeEllipsis.length / stepSize)

    _.range(steps).forEach((i) => {
      const patternToFind = beforeEllipsis.slice(i * stepSize * -1)

      if (removedWhitespace.includes(patternToFind)) {
        startAnchorIndex =
          removedWhitespace.indexOf(patternToFind) + patternToFind.length
      }
    })
  }

  // FIND AFTER ANCHOR
  if (afterEllipsis) {
    const steps = Math.ceil(afterEllipsis.length / stepSize)

    _.range(steps).forEach((i) => {
      const patternToFind = afterEllipsis.slice(0, i * stepSize)

      if (removedWhitespace.includes(patternToFind)) {
        endAnchorIndex = removedWhitespace.indexOf(patternToFind)
      }
    })
  }

  return removedWhitespace.slice(startAnchorIndex, endAnchorIndex)
}
