import { DOMWindow, JSDOM } from 'jsdom'
import { beforeEach, describe, expect, it } from 'vitest'
import { DomUtil } from '../utils/domUtil'

describe('dom manipulations', () => {
  const htmlString = `
    <div id="outerMost">
        Hello World
        <div id="node1">
            node1
        </div>
        <div id="node2">
            node2
        </div>
        <div id="node3">
            node3
        </div>
        <div id="node4">
            node4
            <div id="node41">
              node41
            </div>
            <div id="node42">
                node42
                <div id="node421">
                  node421
                </div>
                <div id="node422">
                  node422
                </div>
            </div>
            <div id="node43">
                node43
            </div>
        </div>
    </div>
  `

  let window: DOMWindow
  let html: Document
  beforeEach(() => {
    window = new JSDOM(htmlString).window
    html = window.document
  })

  it('element.insertBefore() should move node41 as child of node2', () => {
    // GIVEN
    const node41 = html.getElementById('node41')
    if (!node41) throw 'node41 not found'

    const node2 = html.getElementById('node2')
    if (!node2) throw 'node2 not found'

    const node4 = html.getElementById('node4')
    if (!node4) throw 'node4 not found'

    // WHEN
    node2.insertBefore(node41, null)

    // THEN
    expect(node2.children[node2.children.length - 1]).toBe(node41)
    expect(node4.querySelector('#node41')).toBeFalsy()
  })

  it.only('element.after() should move node41 to be younger sibling of node2', () => {
    // GIVEN
    const node41 = html.getElementById('node41')
    if (!node41) throw 'node41 not found'

    const node2 = html.getElementById('node2')
    if (!node2) throw 'node2 not found'

    const node4 = html.getElementById('node4')
    if (!node4) throw 'node4 not found'

    const outerMost = html.getElementById('outerMost')
    if (!outerMost) throw 'outerMost not found'

    // WHEN
    node2.after(node41)

    // THEN
    expect(node41.parentElement).toBe(outerMost)
    expect(DomUtil.getSiblingIndex(node41)).toBe(2)
  })

  it('element.after() should insert newDiv as younger sibling of node2', () => {
    // GIVEN
    const node2 = html.getElementById('node2')
    if (!node2) throw 'node2 not found'

    const outerMost = html.getElementById('outerMost')
    if (!outerMost) throw 'outerMost not found'

    const newDiv = html.createElement('div')
    const newContent = html.createTextNode('Hi there and greetings!')
    newDiv.appendChild(newContent)

    // WHEN
    node2.after(newDiv)

    // THEN
    const children = Array.from(outerMost.children)
    expect(children.indexOf(newDiv)).toBe(2)
  })

  it('element.before() should insert newDiv as older sibling of node2', () => {
    // GIVEN
    const node2 = html.getElementById('node2')
    if (!node2) throw 'node2 not found'

    const outerMost = html.getElementById('outerMost')
    if (!outerMost) throw 'outerMost not found'

    const newDiv = html.createElement('div')
    const newContent = html.createTextNode('Hi there and greetings!')
    newDiv.appendChild(newContent)

    // WHEN
    node2.before(newDiv)

    // THEN
    const children = Array.from(outerMost.children)
    expect(children.indexOf(newDiv)).toBe(1)
  })

  it('element.innerHtml should be usable to create Element instances from htmlStrings', () => {
    // GIVEN
    const outerMost = html.getElementById('outerMost')
    if (!outerMost) throw 'outerMost not found'

    // const newDiv = html.createElement('div')
    // newDiv.innerHTML = '<div>Hi there and greetings!</div>'.trim()
    const domParser = new window.DOMParser()
    const newDiv = domParser.parseFromString(
      '<div>Hi there and greetings!</div>',
      'text/html'
    ).body.firstElementChild

    // WHEN
    outerMost.appendChild(newDiv!)

    // THEN
    const children = Array.from(outerMost.children)
    // expect(children.indexOf(newDiv)).toBe(1)
  })

  it('chapter4.children should be of length 5', () => {
    // GIVEN
    const node4 = html.getElementById('node4')
    if (!node4) throw 'node4 not found'

    // WHEN
    const children = DomUtil.getAllChildrenDF(node4)

    // THEN
    expect(children.length).toBe(5)
    children.forEach((child) => {
      const childIds = ['node41', 'node42', 'node421', 'node422', 'node43']
      expect(childIds.includes(child.id)).toBe(true)
    })
  })

  it('should create empty document', () => {
    // GIVEN

    // WHEN
    const window = new JSDOM().window
    const html = window.document

    // THEN
    expect(html.documentElement.innerHTML).toBe('<head></head><body></body>')
  })
})
