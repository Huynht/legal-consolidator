import { describe, expect, it } from 'vitest'
import { ChapterKey } from '../classes/ChapterKey'

describe('ChapterKey', () => {
  // GET NUMBERING
  describe('numbering', () => {
    it('should find numbering "2.4.5.1" from "Annex II paragraph 2.4.5.1"', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.1')

      // THEN
      expect(ChapterKey.getNumbering(chapterKey)).toBe('2.4.5.1')
    })

    it('should find numbering "2.4.5.1." from "Annex II paragraph 2.4.5.1."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.1.')

      // THEN
      expect(ChapterKey.getNumbering(chapterKey)).toBe('2.4.5.1.')
    })
  })

  // GET PARENT ID
  describe('parentId', () => {
    it('should find parent "2.2." from "2.2.1."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('2.2.1.')

      // THEN
      expect(ChapterKey.generateParentKey(chapterKey)).toBeTruthy()
      expect(ChapterKey.generateParentKey(chapterKey)!.value).toBe('2.2.')
    })

    it('should find parent "Annex II paragraph 2.4.5" from "Annex II paragraph 2.4.5.1"', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.1')

      // THEN
      expect(ChapterKey.generateParentKey(chapterKey)).toBeTruthy()
      expect(ChapterKey.generateParentKey(chapterKey)!.value).toBe(
        'Annex II paragraph 2.4.5'
      )
    })

    it('should find parent "Annex 5 paragraph 2.4.5." from "Annex II paragraph 2.4.5.1."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.1.')

      // THEN
      expect(ChapterKey.generateParentKey(chapterKey)).toBeTruthy()
      expect(ChapterKey.generateParentKey(chapterKey)!.value).toBe(
        'Annex II paragraph 2.4.5.'
      )
    })

    it('should not find parent for "1. Introduction"', () => {
      // GIVEN
      const chapterKey = new ChapterKey('1. Introduction')

      // THEN
      expect(ChapterKey.generateParentKey(chapterKey)).toBe(undefined)
    })

    it('should not find parent for "Annex 5"', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex 5')

      // THEN
      expect(ChapterKey.generateParentKey(chapterKey)).toBe(undefined)
    })
  })

  // GET YOUNGER SIBLING ID
  describe('youngerSiblingId', () => {
    it('should find younger sibling "2.1." from "2.2."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('2.2.')

      // THEN
      expect(
        ChapterKey.generateYoungerSiblingKey(chapterKey)!.value
      ).toBeTruthy()
      expect(ChapterKey.generateYoungerSiblingKey(chapterKey)!.value).toBe(
        '2.1.'
      )
    })

    it('should find younger sibling "2.17." from "2.18."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('2.18.')

      // THEN
      expect(
        ChapterKey.generateYoungerSiblingKey(chapterKey)!.value
      ).toBeTruthy()
      expect(ChapterKey.generateYoungerSiblingKey(chapterKey)!.value).toBe(
        '2.17.'
      )
    })

    it('should find younger sibling "Annex II paragraph 2.4.5.1" from "Annex II paragraph 2.4.5.2"', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.2')

      // THEN
      expect(
        ChapterKey.generateYoungerSiblingKey(chapterKey)!.value
      ).toBeTruthy()
      expect(ChapterKey.generateYoungerSiblingKey(chapterKey)!.value).toBe(
        'Annex II paragraph 2.4.5.1'
      )
    })

    it('should find younger sibling "Annex II paragraph 2.4.5.2." from "Annex II paragraph 2.4.5.3." (ending on .)', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.3.')

      // THEN
      expect(
        ChapterKey.generateYoungerSiblingKey(chapterKey)!.value
      ).toBeTruthy()
      expect(ChapterKey.generateYoungerSiblingKey(chapterKey)!.value).toBe(
        'Annex II paragraph 2.4.5.2.'
      )
    })

    it('should not find younger sibling from "Annex II paragraph 2.4.5.1."', () => {
      // GIVEN
      const chapterKey = new ChapterKey('Annex II paragraph 2.4.5.1.')

      // THEN
      expect(ChapterKey.generateYoungerSiblingKey(chapterKey)).toBe(undefined)
    })
  })
})
