import fs from 'fs'
import { beforeEach, describe, expect, it } from 'vitest'
import { ChapterKey } from '../classes/ChapterKey'
import { ConsolidationEngine } from '../classes/ConsolidationEngine'
import { AmendingInstruction } from '../types'
import { DomUtil } from '../utils/domUtil'

describe('ConsolidationEngine', () => {
  // Setup
  let engine: ConsolidationEngine
  beforeEach(() => {
    const htmlString152 = fs.readFileSync(
      '/home/an/coding/legal-consolidator/src/tests/data/UNR152_0.0.0_enforced.html',
      'utf8'
    )
    const htmlString46 = fs.readFileSync(
      '/home/an/coding/legal-consolidator/src/tests/data/UNR46_4.0.0_enforced.html',
      'utf8'
    )
    const htmlString13H = fs.readFileSync(
      '/home/an/coding/legal-consolidator/src/tests/data/UNR13H_0.15.0_enforced.html',
      'utf8'
    )
    const ce = new ConsolidationEngine()
    ce.declareBaseDocument('UNR152/0.0.0/enforced', htmlString152)
    ce.declareBaseDocument('UNR46/0.0.0/enforced', htmlString46)
    ce.declareBaseDocument('UNR13H/0.0.0/enforced', htmlString13H)
    engine = ce
  })

  describe('general operations', () => {
    it('should handle UNR46/4.1.0/proposed/p1', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'REPLACE_PARTLY',
          newContent: [
            '15.2.4.5.11. The area prescribed in',
            '...',
            'indirect vision devices (of Class IV, V, VI).',
          ],
          targetChapterKey: {
            value: '15.2.4.5.11.',
          },
        },
        {
          type: 'INSERT',
          newContent:
            '<div>15.2.4.5.11.1. If an indirect vision device of Class IV is used to provide a part of the field of vision prescribed in paragraphs 15.2.4.5.6. to 15.2.4.5.9., it shall be adjusted in a way that it simultaneously provides the field of vision prescribed in paragraph 15.2.4.4.2.</div>',
          targetChapterKey: {
            value: '15.2.4.5.11.1.',
          },
        },
        {
          type: 'INSERT',
          newContent:
            '<div>15.2.4.5.11.2. If an indirect vision device of Class V is used to provide a part of the field of vision prescribed in paragraphs 15.2.4.5.6. to 15.2.4.5.9., it shall be adjusted in a way that it simultaneously provides the field of vision prescribed in paragraphs 15.2.4.5.1. to 15.2.4.5.4.</div>',
          targetChapterKey: {
            value: '15.2.4.5.11.2.',
          },
        },
        {
          type: 'INSERT',
          newContent:
            '<div>15.2.4.5.11.3. If an indirect vision device of Class VI is used to provide a part of the field of vision prescribed in paragraphs 15.2.4.5.6. to 15.2.4.5.9., it shall be adjusted in a way that it simultaneously provides the field of vision prescribed in paragraph 15.2.4.6.1.</div>',
          targetChapterKey: {
            value: '15.2.4.5.11.3.',
          },
        },
        {
          type: 'INSERT',
          newContent:
            '<div>15.2.4.5.12. The field of vision prescribed in paragraphs 15.2.4.5.1. to 15.2.4.5.4. may be viewed using a combination of a close-proximity exterior mirror (Class V) and a "wide-angle" exterior mirror (Class IV). In such cases the close-proximity exterior mirror (Class V) shall provide at least 90 per cent of the field of vision prescribed in paragraphs 15.2.4.5.1 to 15.2.4.5.4. and the Class IV mirror shall be adjusted in a way that it simultaneously provides the field of vision prescribed in paragraph 15.2.4.4.2.</div>',
          targetChapterKey: {
            value: '15.2.4.5.12.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: '15.2.4.5.12.',
            },
            to: {
              value: '15.2.4.5.13.',
            },
          },
          newKeys: {
            from: {
              value: '15.2.4.5.13.',
            },
            to: {
              value: '15.2.4.5.14.',
            },
          },
        },
        {
          type: 'REPLACE',
          newContent:
            '<div>15.2.4.5.13. Paragraphs 15.2.4.5.6. to 15.2.4.5.12. shall not apply to a vehicle where any part of the Class V mirror, or its holder, is less than 2.4 m above the ground, regardless of its position after adjustment.</div>',
          targetChapterKey: {
            value: '15.2.4.5.13.',
          },
        },
        {
          type: 'REPLACE_PARTLY',
          newContent: [
            'Paragraphs 15.2.4.5.6. to 15.2.4.5.12. shall not apply',
            '...',
            'M2 or M3.',
          ],
          targetChapterKey: {
            value: '15.2.4.5.14.',
          },
        },
        {
          type: 'DELETE',
          targetChapterKey: {
            value: '21.10.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: '21.11.',
            },
            to: {
              value: '21.16.',
            },
          },
          newKeys: {
            from: {
              value: '21.10.',
            },
            to: {
              value: '21.15.',
            },
          },
        },
        {
          type: 'RENUMBER',
          targetChapterKey: {
            value: '21.17.',
          },
          newChapterKey: {
            value: '21.16.',
          },
        },
        {
          type: 'REPLACE',
          newContent:
            '<div>21.16. Contracting Parties applying this Regulation shall not refuse to grant extensions of type approvals for existing types of vehicles or devices, which are not affected by the 04 series of amendments, granted according to the 02 or 03 series of amendments to this Regulation.</div>',
          targetChapterKey: {
            value: '21.16.',
          },
        },
        {
          type: 'INSERT',
          newContent:
            '<div>21.17. Notwithstanding the provisions of paragraphs 21.2., 21.4., 21.5., 21.13. and 21.15. above, for the purpose of replacement parts, Contracting Parties applying this Regulation shall continue to grant approvals according to the 01 series of amendments to this Regulation, to devices for indirect vision of classes I to V for use on vehicle types which have been approved before 26 January 2006 pursuant to the 01 series of amendments of Regulation No. 46 and, where applicable, subsequent extensions to these approvals.</div>',
          targetChapterKey: {
            value: '21.17.',
          },
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR46/0.0.0/enforced',
        'UNR46/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html.documentElement.innerHTML
      const index152451 = newHtml.indexOf('chapter-key="15.2.4.5.11."')
      const index2110 = newHtml.indexOf('chapter-key="21.10."')

      // console.log(newHtml.slice(index2110 - 200, index2110 + 5000))
    })

    it('should consolidate 3 DELETE instructions correctly', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: new ChapterKey('2.2.'),
        },
        { type: 'DELETE', targetChapterKey: new ChapterKey('2.3.') },
        {
          type: 'DELETE',
          targetChapterKey: new ChapterKey('Annex 3 chapter 2.3.'),
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      // chapter 2.2.
      expect(newHtml.getElementById('635ba01a6c73b11a7c78b6c8')).toBeFalsy()
      // chapter 2.3.
      expect(newHtml.getElementById('635ba01a6c73b11a7c78b6c9')).toBeFalsy()
      // annex 3 chapter 2.3.
      expect(newHtml.getElementById('635ba01a6c73b11a7c78b76b')).toBeFalsy()
    })

    it('should consolidate 1 INSERT', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'INSERT',
          targetChapterKey: new ChapterKey('2.2.1.'),
          newContent: '<div>2.2.1. <b>THIS IS NEW CONTENT</b></div>',
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const newElement = newHtml.querySelectorAll('[chapter-key="2.2.1."]')[0]
      expect(newElement).toBeTruthy()
      expect(newElement.textContent).toBe('2.2.1. THIS IS NEW CONTENT')
    })
  })

  describe('consolidation order', () => {
    it('should consolidate 1 DELETE and 1 INSERT with the same identifier correctly', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: new ChapterKey('2.2.'),
        },
        {
          type: 'INSERT',
          targetChapterKey: new ChapterKey('2.2.'),
          newContent: '<div>2.2. <b>THIS IS NEW CONTENT</b></div>',
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      expect(newHtml.getElementById('635ba01a6c73b11a7c78b6c8')).toBeFalsy()
      const updatedElement = newHtml.querySelectorAll('[chapter-key="2.2."]')[0]
      expect(updatedElement).toBeTruthy()
      expect(updatedElement.textContent).toBe('2.2. THIS IS NEW CONTENT')
    })
  })

  describe('consolidate from seralized data', () => {
    it('should consolidate 1 INSERT correctly from serialized data', () => {
      // GIVEN
      const instructionsString = `[
          {
              "type": "INSERT",
              "targetChapterKey": {
                  "value": "2.2.1."
              },
              "newContent": "<div>2.2.1. <b>THIS IS NEW CONTENT</b></div>"
          }
        ]`

      const instructions: AmendingInstruction[] = JSON.parse(instructionsString)

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const newElement = newHtml.querySelectorAll('[chapter-key="2.2.1."]')[0]
      expect(newElement).toBeTruthy()
      expect(newElement.textContent).toBe('2.2.1. THIS IS NEW CONTENT')
    })

    it('should consolidate 1 DELETE and 1 INSERT with the same identifier correctly from serialized data', () => {
      // GIVEN
      const instructionsString = `[
          {
              "type": "DELETE",
              "targetChapterKey": {
                  "value": "2.2."
              }
          },
          {
              "type": "INSERT",
              "targetChapterKey": {
                  "value": "2.2."
              },
              "newContent": "<div>2.2. <b>new content goes here</b></div>"
          }
        ]`

      const instructions: AmendingInstruction[] = JSON.parse(instructionsString)

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      expect(newHtml.getElementById('635ba01a6c73b11a7c78b6c8')).toBeFalsy()
      const updatedElement = newHtml.querySelectorAll('[chapter-key="2.2."]')[0]
      expect(updatedElement).toBeTruthy()
      expect(updatedElement.textContent).toBe('2.2. new content goes here')
    })
  })

  describe('RENUMBER & RENUMBER_MULTIPLE', () => {
    it('1x DELETE => RENUMBER_MULTIPLE', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: {
            value: '21.10.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: '21.11.',
            },
            to: {
              value: '21.17.',
            },
          },
          newKeys: {
            from: {
              value: '21.10.',
            },
            to: {
              value: '21.16.',
            },
          },
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR46/0.0.0/enforced',
        'UNR46/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const chapters: (Element | null)[] = []
      for (let i = 10; i <= 16; i++) {
        chapters.push(newHtml.querySelector(`[chapter-key="21.${i}."]`))
      }
      chapters.forEach((chapter) => {
        expect(chapter).toBeTruthy()
      })
      expect(newHtml.querySelector(`[chapter-key="21.17."]`)).toBeFalsy()
    })

    it('multiple DELETEs followed by RENUMBER_MULTIPLE', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.1.2.',
          },
        },
        {
          type: 'DELETE',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.1.3.',
          },
        },
        {
          type: 'DELETE',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.1.4.',
          },
        },
        {
          type: 'DELETE',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.1.5.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: 'Annex 9 Part A. chapter 3.4.1.6.',
            },
            to: {
              value: 'Annex 9 Part A. chapter 3.4.1.9.',
            },
          },
          newKeys: {
            from: {
              value: 'Annex 9 Part A. chapter 3.4.1.2.',
            },
            to: {
              value: 'Annex 9 Part A. chapter 3.4.1.5.',
            },
          },
        },
        {
          type: 'REPLACE_KEYWORD',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.',
          },
          keyword: 'paragraph 3.4.1.7.',
          replacement: 'paragraph 3.4.1.3.',
          recursive: true,
        },
        {
          type: 'REPLACE_KEYWORD',
          targetChapterKey: {
            value: 'Annex 9 Part A. chapter 3.4.',
          },
          keyword: 'paragraph 3.4.1.9.',
          replacement: 'paragraph 3.4.1.5.',
          recursive: true,
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR13H/0.0.0/enforced',
        'UNR13H/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html.documentElement.innerHTML
      const index3412 = newHtml.indexOf(
        'chapter-key="Annex 9 Part A. chapter 3.4.1.1."'
      )
      // cons89ole.log(newHtml.slice(index3412, index3412 + 5000))
    })

    it('should handle 3 seperate RENUMBER instructions with overlapping identifiers', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('2.16.'),
          newChapterKey: new ChapterKey('2.17.'),
        },
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('2.17.'),
          newChapterKey: new ChapterKey('2.18.'),
        },
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('2.18.'),
          newChapterKey: new ChapterKey('2.19.'),
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
    })

    it('should handle RENUMBER after RENUMBER_MULTIPLE', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: {
            value: '21.10.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: '21.11.',
            },
            to: {
              value: '21.16.',
            },
          },
          newKeys: {
            from: {
              value: '21.10.',
            },
            to: {
              value: '21.15.',
            },
          },
        },
        {
          type: 'RENUMBER',
          targetChapterKey: {
            value: '21.17.',
          },
          newChapterKey: {
            value: '21.16.',
          },
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR46/0.0.0/enforced',
        'UNR46/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const chapters: (Element | null)[] = []
      const index2110 = newHtml.documentElement.innerHTML.indexOf(
        'chapter-key="21.10."'
      )

      for (let i = 10; i <= 15; i++) {
        chapters.push(newHtml.querySelector(`[chapter-key="21.${i}."]`))
      }
      chapters.forEach((chapter, i) => {
        if (!chapter) {
          console.error('chapter not found: ', i)
        }
        expect(chapter).toBeTruthy()
      })
      expect(newHtml.querySelector(`[chapter-key="21.17."]`)).toBeFalsy()
    })

    it('should handle INSERT + RENUMBER instructions with overlapping identifiers', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'INSERT',
          targetChapterKey: new ChapterKey('2.17.'),
          newContent: '<div>2.17. This is the newly inserted chapter.</div>',
        },
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('2.17.'),
          newChapterKey: new ChapterKey('2.18.'),
        },
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('2.18.'),
          newChapterKey: new ChapterKey('2.19.'),
        },
        {
          type: 'REPLACE',
          targetChapterKey: new ChapterKey('2.19.'),
          newContent: '<div>2.19. Renumbered and amended chapter.</div>',
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR152/0.0.0/enforced',
        'UNR152/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html

      const newChapter217 = newHtml.querySelector('[chapter-key="2.17."]')
      expect(newChapter217).toBeTruthy()

      const newChapter218 = newHtml.querySelector('[chapter-key="2.18."]')
      expect(newChapter218).toBeTruthy()
      expect(newChapter218!.textContent!.trim().startsWith('2.18')).toBeTruthy()

      const newChapter219 = newHtml.querySelector('[chapter-key="2.19."]')
      expect(newChapter219).toBeTruthy()
      expect(newChapter219!.textContent!.trim().startsWith('2.19')).toBeTruthy()
      expect(
        newChapter219?.textContent?.includes('Renumbered and amended chapter')
      ).toBeTruthy()
    })

    it('should handle DELETE + RENUMBER_MULTIPLE with overlapping identifiers', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'DELETE',
          targetChapterKey: {
            value: '21.10.',
          },
        },
        {
          type: 'RENUMBER_MULTIPLE',
          formerKeys: {
            from: {
              value: '21.11.',
            },
            to: {
              value: '21.16.',
            },
          },
          newKeys: {
            from: {
              value: '21.10.',
            },
            to: {
              value: '21.15.',
            },
          },
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR46/0.0.0/enforced',
        'UNR46/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const innerHTML = newHtml.documentElement.innerHTML

      const chapter219Index = innerHTML.indexOf('chapter-key="21.9."')
    })

    it('should handle INSERT + RENUMBER with overlapping identifiers', () => {
      // GIVEN
      const instructions: AmendingInstruction[] = [
        {
          type: 'INSERT',
          targetChapterKey: new ChapterKey('15.2.4.5.12.'),
          newContent:
            '<div>15.2.4.5.12. The field of vision prescribed in paragraphs 15.2.4.5.1. to 15.2.4.5.4. may be viewed using a combination of a close-proximity exterior mirror (Class V) and a "wide-angle" exterior mirror (Class IV). In such cases the close-proximity exterior mirror (Class V) shall provide at least 90 per cent of the field of vision prescribed in paragraphs 15.2.4.5.1 to 15.2.4.5.4. and the Class IV mirror shall be adjusted in a way that it simultaneously provides the field of vision prescribed in paragraph 15.2.4.4.2.</div>',
        },
        {
          type: 'RENUMBER',
          targetChapterKey: {
            value: '15.2.4.5.12.',
          },
          newChapterKey: new ChapterKey('15.2.4.5.13.'),
        },
        {
          type: 'RENUMBER',
          targetChapterKey: new ChapterKey('15.2.4.5.13.'),
          newChapterKey: new ChapterKey('15.2.4.5.14.'),
        },
      ]

      // WHEN
      const newDoc = engine.consolidate(
        'UNR46/0.0.0/enforced',
        'UNR46/0.0.1/proposed/p1',
        instructions
      )

      // THEN
      const newHtml = newDoc.html
      const chapter12 = newHtml.querySelector('[chapter-key="15.2.4.5.12."]')
      const chapter13 = newHtml.querySelector('[chapter-key="15.2.4.5.13."]')
      const chapter14 = newHtml.querySelector('[chapter-key="15.2.4.5.14."]')
      expect(chapter12).toBeTruthy()
      expect(chapter13).toBeTruthy()
      expect(chapter14).toBeTruthy()
      if (!chapter12 || !chapter13 || !chapter14) return

      const chapter12Index = DomUtil.getSiblingIndex(chapter12)
      const chapter13Index = DomUtil.getSiblingIndex(chapter13)
      const chapter14Index = DomUtil.getSiblingIndex(chapter14)
      expect(chapter12Index).toBeTruthy()
      expect(chapter13Index).toBeTruthy()
      expect(chapter14Index).toBeTruthy()
      if (!chapter12Index || !chapter13Index || !chapter14Index) return

      expect(chapter12Index < chapter13Index).toBeTruthy()
      expect(chapter13Index < chapter14Index).toBeTruthy()
    })
  })
})
