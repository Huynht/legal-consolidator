import fs from 'fs'
import { JSDOM } from 'jsdom'
import _ from 'lodash'
import path from 'path'
import { beforeEach, describe, expect, it, SpyInstance, vi } from 'vitest'
import { BaseDocument } from '../classes/BaseDocument'
import { ChapterKey } from '../classes/ChapterKey'
import {
  InsertInstruction,
  RenumberInstruction,
  ReplaceInstruction,
  ReplaceKeywordInstruction,
  ReplacePartlyInstruction,
} from '../types'

describe('BaseDocument', () => {
  let baseDoc152: BaseDocument
  let baseDoc46: BaseDocument
  let consoleErrorSpy: SpyInstance
  let originalKeys152: Map<string, Element>

  beforeEach(() => {
    // init baseDoc152
    const htmlString152 = fs.readFileSync(
      path.resolve(__dirname, './data/UNR152_0.0.0_enforced.html'),
      'utf8'
    )
    const html152 = new JSDOM(htmlString152).window.document
    baseDoc152 = new BaseDocument({
      html: html152,
      docVersionKey: 'UNR152/0.0.0/enforced',
    })

    // init original keys map for 152
    const chapters = html152.querySelectorAll('[chapter-key]')
    originalKeys152 = new Map()

    chapters.forEach((chapter) => {
      const chapterKeyString = chapter.getAttribute('chapter-key')
      if (chapterKeyString) {
        originalKeys152.set(chapterKeyString, chapter)
      }
    })

    // init baseDoc46
    const htmlString46 = fs.readFileSync(
      path.resolve(__dirname, './data/UNR46_4.0.0_enforced.html'),
      'utf8'
    )
    const html46 = new JSDOM(htmlString46).window.document
    baseDoc46 = new BaseDocument({
      html: html46,
      docVersionKey: 'UNR46/0.0.0/enforced',
    })

    // init consoleErrorSpy
    consoleErrorSpy = vi.spyOn(console, 'error')
  })

  describe('findByChapterKey', () => {
    it('should find chapter 2.2 with identifier "2.2"', () => {
      // GIVEN

      // WHEN
      const foundChapter = baseDoc152.findByChapterKey(new ChapterKey('2.2.'))

      // THEN
      expect(foundChapter).toBeTruthy()
      expect(foundChapter!.id).toBe('635ba01a6c73b11a7c78b6c8')
    })

    it('should find chapter "Annex 3 2.2" with identifier "Annex 3 chapter 2.2"', () => {
      // GIVEN

      // WHEN
      const foundChapter = baseDoc152.findByChapterKey(
        new ChapterKey('Annex 3 chapter 2.2.')
      )

      // THEN
      expect(foundChapter).toBeTruthy()
      expect(foundChapter!.id).toBe('635ba01a6c73b11a7c78b76a')
    })
  })

  describe('insertNewChapter', () => {
    it('should insert new chapter 2.2.1. as child of 2.2.', () => {
      // GIVEN
      const insertInstruction: InsertInstruction = {
        type: 'INSERT',
        targetChapterKey: new ChapterKey('2.2.1.'),
        newContent: '<div>2.2.1. <b>this is new content</b></div>',
      }

      // WHEN
      baseDoc152.amend(insertInstruction)

      // THEN
      const html = baseDoc152.html
      const chapter22 = html.querySelector(`[chapter-key='2.2.']`)
      const chapter221 = html.querySelector(`[chapter-key='2.2.1.']`)
      expect(chapter221).toBeTruthy()
      expect(chapter221?.parentElement?.attributes).toBe(chapter22?.attributes)
    })

    it('should insert new chapter 2.2.1. as child of 2.2. from serialized data', () => {
      // GIVEN
      const insertInstructionString = `{
        "type": "INSERT",
        "targetChapterKey": {
            "value": "2.2.1."
        },
        "newContent": "<div>2.2.1. <b>new content goes here</b></div>"
      }`
      const insertInstruction: InsertInstruction = JSON.parse(
        insertInstructionString
      )

      // WHEN
      baseDoc152.amend(insertInstruction)

      // THEN
      const html = baseDoc152.html
      const chapter22 = html.querySelector(`[chapter-key='2.2.']`)
      const chapter221 = html.querySelector(`[chapter-key='2.2.1.']`)
      expect(chapter221).toBeTruthy()
      expect(chapter221?.parentElement?.attributes).toBe(chapter22?.attributes)
    })
  })

  describe('replaceChapter', () => {
    it('should replace content of chapter 2.2. with new content', () => {
      // GIVEN
      const instruction: ReplaceInstruction = {
        type: 'REPLACE',
        targetChapterKey: new ChapterKey('2.2.'),
        newContent: '<div>2.2. This is new content.</div>',
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      const chapter22 = baseDoc152.html.querySelector('[chapter-key="2.2."]')
      expect(chapter22).toBeTruthy()
      expect(chapter22?.textContent).toBe('2.2. This is new content.')
    })
  })

  describe('renumberChapter', () => {
    it('should renumber chapter 2.18. to 2.19. while swapping out the identifier in the text', () => {
      // GIVEN
      const targetKey = new ChapterKey('2.18.')
      const newKey = new ChapterKey('2.19.')
      const targetChapter = baseDoc152.findByChapterKey(targetKey)
      let content = ''
      if (targetChapter) {
        content = targetChapter.textContent!.trim()
      }
      expect(content).not.toBe('')

      const instruction: RenumberInstruction = {
        type: 'RENUMBER',
        targetChapterKey: targetKey,
        newChapterKey: newKey,
      }

      // WHEN
      baseDoc152.amend(instruction, originalKeys152)

      // THEN
      const renumberedChapterElement = baseDoc152.html.querySelector(
        `[chapter-key='2.19.']`
      )
      expect(renumberedChapterElement).toBeTruthy()
      expect(renumberedChapterElement?.textContent!.trim()).toBe(
        content.replace(targetKey.value, newKey.value)
      )
    })

    it('should renumber chapter 2.17. to 2.19. while swapping out the identifier in the text', () => {
      // GIVEN
      const targetKey = new ChapterKey('2.17.')
      const newKey = new ChapterKey('2.19.')
      const targetChapter = baseDoc152.findByChapterKey(targetKey)
      let content = ''
      if (targetChapter) {
        content = targetChapter.textContent!.trim()
      }
      expect(content).not.toBe('')

      const instruction: RenumberInstruction = {
        type: 'RENUMBER',
        targetChapterKey: targetKey,
        newChapterKey: newKey,
      }

      // WHEN
      baseDoc152.amend(instruction, originalKeys152)

      // THEN
      const renumberedChapterElement = baseDoc152.html.querySelector(
        `[chapter-key='2.19.']`
      )
      expect(renumberedChapterElement).toBeTruthy()
      expect(renumberedChapterElement?.textContent!.trim()).toBe(
        content.replace(targetKey.value, newKey.value)
      )
      // TODO: check for correct positioning of chapter after renumbering
    })

    it('should renumber chapter 3.2. to 3.4. while retaining children of the moved chapter', () => {
      // GIVEN
      const targetKey = new ChapterKey('3.2.')
      const newKey = new ChapterKey('3.4.')
      const targetChapter = baseDoc152.findByChapterKey(targetKey)
      let content = ''
      if (targetChapter) {
        content = targetChapter.textContent!.trim()
      }
      expect(content).not.toBe('')
      const childCountBeforeMove = targetChapter?.children.length

      const instruction: RenumberInstruction = {
        type: 'RENUMBER',
        targetChapterKey: targetKey,
        newChapterKey: newKey,
      }

      // WHEN
      baseDoc152.amend(instruction, originalKeys152)

      // THEN
      const renumberedChapterElement =
        baseDoc152.html.querySelector(`[chapter-key='3.4.']`)
      expect(renumberedChapterElement).toBeTruthy()
      expect(renumberedChapterElement?.children.length).toBe(
        childCountBeforeMove
      )
      expect(renumberedChapterElement?.textContent!.trim()).toBe(
        content.replace(targetKey.value, newKey.value)
      )
    })
  })

  describe('replacePartly', () => {
    it('should replace "Emergency Braking" with "BLABLA BRAKING" while using before and after ellipsis', () => {
      // GIVEN
      const targetChapterKey = new ChapterKey('2.2.')
      const targetChapter = baseDoc152.findByChapterKey(targetChapterKey)

      // ORIGINAL HTML
      // <div>2.2. <emph>Emergency Braking</emph> means a braking demand emitted by the AEBS to the service braking system of the vehicle.</div>
      const originalTextContent = targetChapter!.textContent

      const instruction: ReplacePartlyInstruction = {
        type: 'REPLACE_PARTLY',
        targetChapterKey: targetChapterKey,
        newContent: [
          '2.2. "BLABLA BRAKING" means',
          '...',
          'braking system of the vehicle.',
        ],
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      expect(targetChapter?.textContent?.trim()).toBe(
        originalTextContent
          ?.replace('Emergency Braking', 'BLABLA BRAKING')
          .trim()
          .replace(/\s+/g, ' ')
      )
      expect(targetChapter?.innerHTML.includes('<b>AEBS</b>')).toBe(true)
    })

    it('should replace "Emergency Braking" with "BLABLA BRAKING" while using only before ellipsis', () => {
      // GIVEN
      const targetChapterKey = new ChapterKey('2.2.')
      const targetChapter = baseDoc152.findByChapterKey(targetChapterKey)

      // ORIGINAL HTML
      // <div>2.2. <emph>Emergency Braking</emph> means a braking demand emitted by the AEBS to the service braking system of the vehicle.</div>
      const originalTextContent = targetChapter!.textContent

      const instruction: ReplacePartlyInstruction = {
        type: 'REPLACE_PARTLY',
        targetChapterKey: targetChapterKey,
        newContent: ['2.2. "BLABLA BRAKING" means', '...'],
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      expect(targetChapter?.textContent?.trim()).toBe(
        originalTextContent
          ?.replace('Emergency Braking', 'BLABLA BRAKING')
          .trim()
          .replace(/\s+/g, ' ')
      )
      expect(targetChapter?.innerHTML.includes('<b>AEBS</b>')).toBe(true)
    })

    it('should replace "AEBS" with "ABS" while using only after ellipsis', () => {
      // GIVEN
      const targetChapterKey = new ChapterKey('2.2.')
      const targetChapter = baseDoc152.findByChapterKey(targetChapterKey)

      // ORIGINAL HTML
      // <div>2.2. <emph>Emergency Braking</emph> means a braking demand emitted by the AEBS to the service braking system of the vehicle.</div>
      const originalTextContent = targetChapter!.textContent

      const instruction: ReplacePartlyInstruction = {
        type: 'REPLACE_PARTLY',
        targetChapterKey: targetChapterKey,
        newContent: [
          '...',
          'demand emitted by the <b>ABS</b> to the service braking system of the vehicle.',
        ],
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      expect(targetChapter?.textContent?.trim()).toBe(
        originalTextContent?.replace('AEBS', 'ABS').trim().replace(/\s+/g, ' ')
      )
      expect(targetChapter?.innerHTML.includes('<b>ABS</b>')).toBe(true)
    })

    it('should replace partly in chapter with sub chapters without affecting children', () => {
      // GIVEN
      const instruction: ReplacePartlyInstruction = {
        type: 'REPLACE_PARTLY',
        targetChapterKey: new ChapterKey('2.1.'),
        newContent: [
          '...',
          'devices able to present information about the indirect field of vision to the driver and HERE COME CHANGES.',
        ],
      }

      // WHEN
      baseDoc46.amend(instruction)

      // THEN
      _.range(4).forEach((i) => {
        expect(
          baseDoc46.html.querySelector(`[chapter-key="2.1.${i + 1}."]`)
        ).toBeTruthy()
      })
      // const html = baseDoc46.html.documentElement.innerHTML
      // const index21 = html.indexOf('chapter-key="2.1."')
      // console.log(html.slice(index21 - 50, index21 + 5000))
    })
  })

  describe('replaceKeyword', () => {
    it('should replace "Emergency Braking" with "BLABLA BRAKING"', () => {
      // GIVEN
      const targetChapterKey = new ChapterKey('2.2.')
      const targetChapter = baseDoc152.findByChapterKey(targetChapterKey)
      const originalTextContent = targetChapter!.textContent

      const instruction: ReplaceKeywordInstruction = {
        type: 'REPLACE_KEYWORD',
        keyword: 'Emergency Braking',
        replacement: 'BLABLA BRAKING',
        recursive: false,
        targetChapterKey: new ChapterKey('2.2.'),
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      expect(targetChapter?.textContent?.trim().replace(/\s+/g, ' ')).toBe(
        originalTextContent
          ?.replace('Emergency Braking', 'BLABLA BRAKING')
          .trim()
          .replace(/\s+/g, ' ')
      )
      expect(targetChapter?.innerHTML.includes('<b>AEBS</b>')).toBe(true)
    })

    it('should replace "devices" with "BLABLABLA" with recursive = false', () => {
      // GIVEN
      const keyword = 'devices'
      const replacement = 'BLABLABLA'

      const targetChapterKey = new ChapterKey('2.1.')
      const targetChapter = baseDoc46.findByChapterKey(targetChapterKey)

      const instruction: ReplaceKeywordInstruction = {
        type: 'REPLACE_KEYWORD',
        keyword: keyword,
        replacement: replacement,
        recursive: false,
        targetChapterKey: targetChapterKey,
      }

      // WHEN
      baseDoc46.amend(instruction)

      // THEN
      expect(targetChapter?.innerHTML.includes('means BLABLABLA')).toBe(true)
      expect(targetChapter?.innerHTML.includes('other BLABLABLA')).toBe(true)
      // dont change in child nodes
      expect(targetChapter?.innerHTML.includes('excluding devices')).toBe(true)
      // console.log(baseDoc46.html.documentElement.innerHTML.slice(0, 5000))
    })

    it('should replace "devices" with "BLABLABLA" with recursive = false', () => {
      // GIVEN
      const keyword = 'devices'
      const replacement = 'BLABLABLA'

      const targetChapterKey = new ChapterKey('2.1.')
      const targetChapter = baseDoc46.findByChapterKey(targetChapterKey)

      const instruction: ReplaceKeywordInstruction = {
        type: 'REPLACE_KEYWORD',
        keyword: keyword,
        replacement: replacement,
        recursive: true,
        targetChapterKey: targetChapterKey,
      }

      // WHEN
      baseDoc46.amend(instruction)

      // THEN
      expect(targetChapter?.innerHTML.includes('means BLABLABLA')).toBe(true)
      expect(targetChapter?.innerHTML.includes('other BLABLABLA')).toBe(true)
      expect(targetChapter?.innerHTML.includes('excluding BLABLABLA')).toBe(
        true
      )
    })

    it('should replace "he" with "the" while not touching existing "the"s', () => {
      // GIVEN
      const targetChapterKey = new ChapterKey('10.')
      const targetChapter = baseDoc152.findByChapterKey(targetChapterKey)

      const instruction: ReplaceKeywordInstruction = {
        type: 'REPLACE_KEYWORD',
        keyword: 'he',
        replacement: 'the',
        recursive: false,
        targetChapterKey: targetChapterKey,
      }

      // WHEN
      baseDoc152.amend(instruction)

      // THEN
      expect(targetChapter?.innerHTML.includes('the shall so inform')).toBe(
        true
      )
      expect(targetChapter?.innerHTML.includes('inform the Type')).toBe(true)
      expect(targetChapter?.innerHTML.includes('inform the other')).toBe(true)
    })
  })
})
